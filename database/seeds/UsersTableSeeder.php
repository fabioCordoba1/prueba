<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rootRole = Role::create([
            'name' => 'Administrador',
            'slug' => 'admin',
            'description' => 'Super Usuario, Acceso Total',
            'special' => 'all-access'
        ]);

        $venRole = Role::create([
            'name' => 'Vendedor',
            'slug' => 'vendedor',
            'description' => 'Acceso Limitado',
        ]);

        User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('admin'),
        ]);

        $rootUsers = User::whereIn('id', [1])->get();
        foreach($rootUsers as $rootUser){
            $rootUser->assignRoles($rootRole);
        }

        $roles = Role::whereIn('id',[2])->get();
        foreach($roles as $role){
            $role->givePermissionTo('clientes.index','clientes.show','clientes.create','clientes.edit','clientes.destroy');
        }
    }
}
