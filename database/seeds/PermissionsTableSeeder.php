<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permisos De Usuarios
        Permission::create([
            'name' => 'Navegar users',
            'slug' => 'users.index',
            'description' => 'Lista y navega users',
        ]);

        Permission::create([
            'name' => 'Ver Detalles De users',
            'slug' => 'users.show',
            'description' => 'Ver en detalle cada Marca del sistema',
        ]);
        
        Permission::create([
            'name' => 'Edicion users',
            'slug' => 'users.edit',
            'description' => 'Editar cualquier dato de una marca del sistema',
        ]);
        
        Permission::create([
            'name' => 'Eliminar users',
            'slug' => 'users.destroy',
            'description' => 'Eliminar cualquier usuario del sistema',
        ]);

        // Permisos De Roles
        Permission::create([
            'name' => 'Navegar Roles',
            'slug' => 'roles.index',
            'description' => 'Lista y navega Roles',
        ]);

        Permission::create([
            'name' => 'Ver Detalles De Rol',
            'slug' => 'roles.show',
            'description' => 'Ver en detalle cada Rol del sistema',
        ]);

        Permission::create([
            'name' => 'Crear Rol',
            'slug' => 'roles.create',
            'description' => 'Crear Rol del sistema',
        ]);

        Permission::create([
            'name' => 'Edicion Rol',
            'slug' => 'roles.edit',
            'description' => 'Editar cualquier dato de una Rol del sistema',
        ]);

        Permission::create([
            'name' => 'Eliminar Rol',
            'slug' => 'roles.destroy',
            'description' => 'Eliminar cualquier Rol del sistema',
        ]);

        // Permisos De Clientes
        Permission::create([
            'name' => 'Navegar cliente',
            'slug' => 'clientes.index',
            'description' => 'Lista y navega cliente',
        ]);

        Permission::create([
            'name' => 'Ver Detalles De clientes',
            'slug' => 'clientes.show',
            'description' => 'Ver en detalle cada Cliente del sistema',
        ]);

        Permission::create([
            'name' => 'Crear clientes',
            'slug' => 'clientes.create',
            'description' => 'Crear Clientes del sistema',
        ]);

        Permission::create([
            'name' => 'Edicion clientes',
            'slug' => 'clientes.edit',
            'description' => 'Editar cualquier dato de un Cliente del sistema',
        ]);

        Permission::create([
            'name' => 'Eliminar clientes',
            'slug' => 'clientes.destroy',
            'description' => 'Eliminar cualquier Cliente del sistema',
        ]);
       
       
    }
}
