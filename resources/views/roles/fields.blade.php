<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <h3>Permiso Especial</h3>
    <div class="form-group">
        <label>{{Form::radio('special', 'all-access')}} Acceso Total</label>
        <label>{{Form::radio('special', 'no-access')}} Sin Acceso</label>
    </div>
</div>

<div class="form-group col-sm-6">
    <h3>Lista de Permisos</h3>
    <ul class="list-untyled">
        @foreach ($permissions as $item)
            <li>
                <label>
                    {{Form::checkbox('permissions[]',$item->id, null)}}
                    {{$item->name}}
                    <em>({{$item->description ?: 'Sin Descripcion'}}) </em>
                </label>
            </li>
        @endforeach
    </ul>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('roles.index') }}" class="btn btn-default">Cancel</a>
</div>
