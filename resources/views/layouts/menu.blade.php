@can('roles.index')
    <li class="{{ Request::is('roles*') ? 'active' : '' }}">
        <a href="{{ route('roles.index') }}"><i class="fas fa-users-cog"></i><span> Roles</span></a>
    </li>
@endcan

@can('users.index')
    <li class="{{ Request::is('users*') ? 'active' : '' }}">
        <a href="{{ route('users.index') }}"><i class="fas fa-users"></i><span> Usuarios</span></a>
    </li>
@endcan





<li class="{{ Request::is('clientes*') ? 'active' : '' }}">
    <a href="{{ route('clientes.index') }}"><i class="fa fa-edit"></i><span>Clientes</span></a>
</li>

