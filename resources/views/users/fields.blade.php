<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <h3>Lista de Roles</h3>
    <ul class="list-untyled">
        @foreach ($roles as $item)
            <li>
                <label>
                    {{Form::checkbox('roles[]',$item->id, null)}}
                    {{$item->name}}
                    <em>({{$item->description ?: 'Sin Descripcion'}}) </em>
                </label>
            </li>
        @endforeach
    </ul>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('users.index') }}" class="btn btn-default">Cancel</a>
</div>
