<?php

namespace App\Http\Controllers;
use Flash;
use App\User;
use Caffeinated\Shinobi\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate();
        return view('users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();
        return view('users.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$users = User::create($request->all());
        $users = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['email']),
        ]);
        return redirect()->route('users.edit',$users->id)
            ->with('info','usuario guardado con exito...');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::find($id);
        return view('users.show',compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::get();
        $users = User::find($id);
        return view('users.edit',compact('users','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $users = User::find($id);
        $users->update($request->all());
        $users->roles()->sync($request->get('roles'));
        Flash::success('Roles updated successfully.');
        return redirect()->route('users.index', $users->id)
            ->with('info','usuario Actualizado con exito...');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return back()->with('info','Usuario Eliminado correctamente');
    }

    public function buscar(Request $request){
        dd($request);
        /*$pacientes=DB::table('pacientes')
        ->where('pacientes.estado',1)
        ->whereRaw($this->GetQuerySearch($request->filtro, $request->texto))
        ->take(10)
        ->get();
        return view('modulos.Ingresos.paciente.fragmentos.pacientes', compact('pacientes'))->render();
        */
    }
}
