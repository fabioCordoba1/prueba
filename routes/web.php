<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('login', 'TokensController@authenticate');
Route::post('register', 'TokensController@register');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->middleware('verified');
Route::get('buscarUser', 'HomeController@buscarUser')->name('buscarUser');

Route::middleware(['auth'])->group(function(){
    //Roles
    Route::post('roles/store', 'rolesController@store')->name('roles.store')
        ->middleware('can:roles.create');

    Route::get('roles', 'rolesController@index')->name('roles.index')
        ->middleware('can:roles.index');

    Route::get('roles/create', 'rolesController@create')->name('roles.create')
        ->middleware('can:roles.create');

    Route::put('roles/{role}', 'rolesController@update')->name('roles.update')
        ->middleware('can:roles.edit');
        
    Route::get('roles/{role}', 'rolesController@show')->name('roles.show')
        ->middleware('can:roles.show');

    Route::delete('roles/{role}', 'rolesController@destroy')->name('roles.destroy')
        ->middleware('can:roles.destroy');

    Route::get('roles/{role}/edit', 'rolesController@edit')->name('roles.edit')
        ->middleware('can:roles.edit');

    //Users
    Route::post('users/store', 'UserController@store')->name('users.store')
        ->middleware('can:users.create');

    Route::get('users', 'UserController@index')->name('users.index')
        ->middleware('can:users.index');

    Route::get('users/create', 'UserController@create')->name('users.create')
        ->middleware('can:users.create');

    Route::put('users/{user}', 'UserController@update')->name('users.update')
        ->middleware('can:users.edit');
        
    Route::get('users/{user}', 'UserController@show')->name('users.show')
        ->middleware('can:users.show');
        
    Route::delete('users/{user}', 'UserController@destroy')->name('users.destroy')
        ->middleware('can:users.destroy');

    Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit')
        ->middleware('can:users.edit');
    //Clientes
    Route::post('clientes/store', 'ClienteController@store')->name('clientes.store')
        ->middleware('can:clientes.create');

    Route::get('clientes', 'ClienteController@index')->name('clientes.index')
        ->middleware('can:clientes.index');

    Route::get('clientes/create', 'ClienteController@create')->name('clientes.create')
        ->middleware('can:clientes.create');

    Route::put('clientes/{cliente}', 'ClienteController@update')->name('clientes.update')
        ->middleware('can:clientes.edit');
        
    Route::get('clientes/{cliente}', 'ClienteController@show')->name('clientes.show')
        ->middleware('can:clientes.show');
        
    Route::delete('clientes/{cliente}', 'ClienteController@destroy')->name('clientes.destroy')
        ->middleware('can:clientes.destroy');

    Route::get('clientes/{cliente}/edit', 'ClienteControllerr@edit')->name('clientes.edit')
        ->middleware('can:clientes.edit');

});

Route::resource('roles', 'rolesController')->middleware('verified');
Route::resource('users', 'UserController')->middleware('verified');
Route::resource('clientes', 'ClienteController')->middleware('verified');